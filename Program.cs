﻿using System;

namespace simple_event_logger
{
    class Program
    {
        static void Main(string[] args)
        {
            //EventLog e = new EventLog("event one");
            //EventLog e2 = new EventLog("event two");
            //EventLog e3 = new EventLog("event three");
            Logger.Log("logger test");
            Logger.Log("Testing Logger class");
        }
    }
}
