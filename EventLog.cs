using System;
using System.IO;
using System.Text;

namespace simple_event_logger
{
    public class EventLog {

        private string file = "Log.txt";
        private string eventMessage;
        private DateTime eventTime = new DateTime();
        StringBuilder logMessage = new StringBuilder();

        public EventLog( string msg ){
            eventMessage = msg;
            System.Console.WriteLine(msg);
            eventTime = DateTime.Now;

            logMessage.AppendFormat("{0}|{1}", eventTime.ToString("MM/dd/yy HH:mm:ss.ff"), eventMessage);

            WriteEventToFile(logMessage);
        }

        private void WriteEventToFile(StringBuilder msg){
            if (File.Exists(file))
            {
                // let's append the file
                System.Console.WriteLine($"Found {file}");
                using (StreamWriter sw = File.AppendText(file))
                {
                    sw.WriteLine(msg);
                    System.Console.WriteLine($"Appended message to {file}");
                }
            }
            else
            {
                System.Console.WriteLine($"File not found, created a new {file}");
                // create new file and then log the message to that file
                using (StreamWriter sw = new StreamWriter(file))
                {
                    sw.WriteLine(msg);
                    System.Console.WriteLine($"Added message to {file}");
                }
            }
        }
    }
}