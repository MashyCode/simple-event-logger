# What is this?
A simple event logger that writes event logs to text file.

# Motivation
I wanted to familiarise myself using git through the command line interface and needed a project to do this. I learned how to use git with GitExtensions, but on the off chance that particular program isn't availble to me, then I would like to fallback to something else. 

# How does it work?
Currently you have to import the Logger.cs and EventLog.cs script to your project and include the namespaces in your project. Type the following anywhere in your code:

```csharp
    Logger.Log("Your event message here");
```

The ```Logger.Log()``` method is a static function. It will create and event object which gets serialised to a text file. The text file by default will be saved in the project root and will be called ```Log.txt```. The script will append new events if the text file exists, otherwise it'll create a new one.